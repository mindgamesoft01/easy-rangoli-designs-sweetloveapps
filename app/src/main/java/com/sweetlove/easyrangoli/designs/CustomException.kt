package com.sweetlove.easyrangoli.designs

class CustomException(message: String) : Exception(message)
